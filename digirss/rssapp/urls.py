from django.conf.urls import url
from rssapp import views
# SET THE NAMESPACE!
app_name = 'rssapp'

urlpatterns=[
    url(r'^main/$', views.main, name='main'),
    url(r'^user_register/$',views.user_register,name='user_register'),
    url(r'^user_login/$',views.user_login,name='user_login'),
]