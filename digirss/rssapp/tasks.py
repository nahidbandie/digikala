from celery import shared_task
from rssapp.utils import update_feeds


@shared_task
def get_those_feeds():
    # the number is the max number of feeds to poll in one go
    update_feeds(30)