from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser


class Feeds(models.Model):
    title = models.CharField(max_length=200)
    link = models.CharField(max_length=2048)
    desc = models.TextField(null=True, blank=True)
    date = models.DateTimeField()
    favourite = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='user_favourite')


class CustomUser(AbstractUser):
    feed = models.ManyToManyField(Feeds, blank=True)
    pass
